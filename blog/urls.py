from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from .import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^$', views.index),  
    url(r'^$', views.post_list),
    url(r'^$', views.login),
    url(r'^$', views.RegistroUsuario),
    path('', views.index, name = 'index'),
    path('', views.post_list, name='post_list'),
    path('', views.login, name = 'login'),
    path('', views.RegistroUsuario, name = 'RegistroUsuario'),

]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)