from django.db import models
from django.utils import timezone
from django.utils.deconstruct import deconstructible
import os
from django.contrib.auth.models import AbstractBaseUser, UserManager
from uuid  import uuid4


@deconstructible
# Create your models here.
class PathAndRename(object):
  def __init__(self,sub_path):
    self.sub_path = sub_path

  def  __call__(self, instance, filename):
    ext=filename.split('.')[-1]
    filename ='{}.{}'.format(uuid4().hex,ext)
    return os.path.join(self.path, filename)

path_and_rename_imagen = PathAndRename("fotos")    

class Post(models.Model):
    correo = models.EmailField(unique=True, blank=True, null=True,max_length=75)
    run = models.CharField(unique=True, max_length=20,null=True)
    nombres = models.CharField(max_length=120,null=True)
    apellidos = models.CharField(max_length=50,null=True)
    telefono = models.CharField(max_length=20,null=True)
    fnacimiento = models.DateField(blank=True, null=True)
    foto = models.ImageField(upload_to=path_and_rename_imagen,null=True)
    region = models.CharField(max_length=120,null=True)
    comuna = models.CharField(max_length=120,null=True)
    direccion = models.CharField(max_length=120,null=True)
    password = models.CharField(max_length=12,null=True)
    password2 = models.CharField(max_length=12,null=True)


class User(models.Model):
   correo = models.CharField(max_length=120)
   password = models.CharField(max_length=12)

#def __str__(self):
  #  return self.user_name
    